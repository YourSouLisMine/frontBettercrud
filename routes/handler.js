const express = require('express');
const handler = express();

let index = require('./index');

handler.use('/', index);

module.exports = handler;