const express = require('express');
const router = express.Router();
const fs = require('fs');
const ejs = require('ejs');

router.get('/', (req, res) => {
    res.render("index");
});

router.get('/login', (req, res) => {
    res.render("login");
});

router.get('/signup', (req, res) => {
    res.render("signup");
});

router.get('/service', (req, res) => {
    res.render("service");
});

module.exports = router;