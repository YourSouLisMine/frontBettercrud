const gulp = require('gulp'),
    sass = require('gulp-sass'),
    ts = require('gulp-typescript'),
    tsProject = ts.createProject('public/libs/typescript/signup/tsconfig.json');

gulp.task('sass', () =>
    gulp.src('./public/sass/*.sass')
    .pipe(sass({
        outputStyle: 'expanded',
        sourceComments: true
    }))
    .pipe(gulp.dest('./public/css'))
);

gulp.task('signup', () => {
    return tsProject.src()
    .pipe(tsProject())
    .js.pipe(gulp.dest('./public/libs/js/signup'));
});

gulp.task('default', () => {
    gulp.watch('./public/sass/*.sass', ['sass'])
    gulp.watch('./public/libs/typescript/signup/*.ts', ['signup'])
});