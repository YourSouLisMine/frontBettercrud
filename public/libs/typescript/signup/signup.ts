class SignUp {

    functionArrayJson():object[] {
        let arrayData:object[] = $('#signup').serializeArray();
        let jsonData:any = {};

        $.map(arrayData, function(field:any) {
            jsonData[field.name] = field.value;
        });

        return jsonData;
    }
}    
