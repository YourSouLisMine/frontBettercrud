class SignUp {
    functionArrayJson() {
        let arrayData = $('#signup').serializeArray();
        let jsonData = {};
        $.map(arrayData, function (field) {
            jsonData[field.name] = field.value;
        });
        return jsonData;
    }
}
