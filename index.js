const express = require('express');
const handler = require('./routes/handler');
const app = express();
const path = require('path');

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

app.use(function(req, res, next) {
    let globalRes = res;
    process.on('unhandledRejection', function(reason, p) {
        console.log(reason);
        try {
            return globalRes.send({
                status: false,
                data: {
                    err: `${reason.type}`,
                    userMsg: 'Error interno.'
                }
            }).sendStatus(500);
        } catch (err) {
            return 0;
        }
    });

    next();
});

app.use('/public', express.static('public'));

let PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Now listening for port ${PORT}`);
});

app.use('/', handler);